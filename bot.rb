require 'telegram/bot'

puts 'start bot'

TOKEN = ENV['TOKEN']
FILE = "facts.txt".freeze

FACTS_ARRAY = [
'Улица Мелик-Адамяна считается самой короткой улицей Еревана',
'Армянский алфавит был создан ученым и священником Месропом Маштоцем в 405 году',
'Центральный стадион «Раздан» — крупнейший стадион Армении. Вместимость арены - 53 849 мест'
]

FACTS_ARRAY.freeze

PORT = (ARGV.first || ENV['PORT'] || 3000).to_i

def choice_facts_array
	array = Array.new
  if File.exist?(FILE) && !File.zero?(FILE)
    array += read_file
  else
    array += FACTS_ARRAY
  end
  return array
end

def read_file
  file = File.open(FILE, "r")
  file_data = file.readlines.map(&:chomp)
  file_data = file_data.reject { |c| c.empty? }
  file.close
  return file_data
end

Telegram::Bot::Client.run(TOKEN) do |bot|
  bot.listen do |message|
    case message
      when Telegram::Bot::Types::CallbackQuery
        kb = [
	Telegram::Bot::Types::InlineKeyboardButton.new(text: 'Узнать случайный факт об Армении', callback_data: 'TelmMeFacts'),
	]
	markup = Telegram::Bot::Types::InlineKeyboardMarkup.new(inline_keyboard: kb)
	# Here you can handle your callbacks from inline buttons
	if message.data == 'TelmMeFacts'
          bot.api.send_message(
          chat_id: message.from.id,
	  text: "#{choice_facts_array.sample}.",
	  reply_markup: markup)
	end
      when Telegram::Bot::Types::Message
        kb = [
        Telegram::Bot::Types::InlineKeyboardButton.new(text: 'Перейти к основному каналу', url: 'https://t.me/politnor'),
	Telegram::Bot::Types::InlineKeyboardButton.new(text: 'Узнать случайный факт об Армении', callback_data: 'TelmMeFacts'),
	]
        markup = Telegram::Bot::Types::InlineKeyboardMarkup.new(inline_keyboard: kb)
        bot.api.send_message(
	  chat_id: message.chat.id,
	  text: "Привет, #{message.from.first_name}!
    	  Чем займемся?",
	  reply_markup: markup)
        end
    end
end

puts 'stop bot'

